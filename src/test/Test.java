package test;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import selvvir.RuntimeCaniuse;
import selvvir.SelvvirCommands;
import selvvir.SelvvirCommands.V;

import com.zwitserloot.json.JSON;

public class Test {

	/**
	 * @param args
	 */
	public static void main1(String[] args) {

		String REGEX_PATTERN = "^s/(?:\\\\/|[^/])*/(?:\\\\/|[^/])*/[igm]*$";
System.out.println(REGEX_PATTERN);

		Pattern REGEX_PARTS = Pattern.compile("(?<![\\\\])/((?:\\\\/|[^/])*)");
System.out.println(REGEX_PARTS.pattern());

		String input = "s/a\\/b/c\\/de/igm";
		//input = "s/1.../2.../i";
System.out.println(input);

		System.out.println("Matches: " + input.matches(REGEX_PATTERN));

		Matcher matcher = REGEX_PARTS.matcher(input);
		System.out.println("  Found: " + matcher.find() + ", " + matcher.group(1));
		System.out.println("  Found: " + matcher.find() + ", " + matcher.group(1));
		System.out.println("  Found: " + matcher.find() + ", " + matcher.group(1));

		Pattern RREGEX_PATTERN = Pattern.compile("^s/((?:\\\\/|[^/])*)/((?:\\\\/|[^/])*)/([igm]*)$");
		matcher = RREGEX_PATTERN.matcher(input);
		System.out.println("Matches: " + matcher.matches());
		System.out.println("  Found: " + matcher.find());
		System.out.println("  Found: " + matcher.group(0));
		System.out.println("  Found: " + matcher.group(1));
		System.out.println("  Found: " + matcher.group(2));
	}

	public static void main2(String[] args) throws UnsupportedEncodingException {

		JSON json = JSON.newMap();
		json.get("longUrl").add().setString("http://validator.w3c.org");
		//json = SelvvirCommands.getResults("https://www.googleapis.com/urlshortener/v1/url", json.toJSON());
		System.out.println(json.toJSON());
	}

	private static String truncate(String url) {

		if (url.length() > 30) {
			return url.substring(0, 20) + "…" + url.substring(url.length() - 10);
		}
		return url;
	}

	public static void main(String[] args) throws UnsupportedEncodingException {

		RuntimeCaniuse json = new RuntimeCaniuse(new File("caniuse/data.json"));
		//System.out.println(json.find("audio", false, false, false, false, false));
		//System.out.println(json.find("audio", true, true, false, false, false));
		//System.out.println(json.find("audio", true, false, false, false, false));
		//System.out.println(json.find("audio", false, true, false, false, false));
		System.out.println(json.find("flex", true, true, false, false, true).get(0).value);
	}
}
