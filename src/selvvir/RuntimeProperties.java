package selvvir;

import java.util.Properties;
import java.util.Set;

import java.io.File;
import java.io.FileInputStream;

/*
 * Care should be taken as this class throws RuntimeExceptions not Exceptions, given that code relying
 * on instances of this class doesn't expect the file it is watching to become unavailable at runtime.
 */
public class RuntimeProperties {

	private final Properties properties;
	private final File file;
	private Long last;

	public RuntimeProperties(String path) {
		this(new File(path));
	}

	public RuntimeProperties(File file) {
		properties = new Properties();
		this.file = file;
		load();
	}

	private void load() {
		try {
			FileInputStream stream = null;
			try {
				stream = new FileInputStream(file);
				properties.clear();
				properties.load(stream);
				last = file.lastModified();
			} finally {
				if(stream != null) {
					stream.close();
				}
			}
		} catch(Exception e) {
			throw new RuntimeException("couldn't reload properties file", e);
		}
	}

	public boolean getBoolean(String key) {
		return "true".equals((getProperty(key, null) + "").toLowerCase());
	}

	public double getNumber(String key) {
		return Double.valueOf(getProperty(key, null));
	}

	public String getProperty(String key) {
		return getProperty(key, null);
	}

	public String getProperty(String key, String defaultValue) {
		synchronized(file) {
			if(file.lastModified() != last) {
				load();
			}
			return properties.getProperty(key, defaultValue);
		}
	}

	public Set<Object> getKeys() {
		synchronized(file) {
			if(file.lastModified() != last) {
				load();
			}
			return properties.keySet();
		}
	}

}
