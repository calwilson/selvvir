package selvvir;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.WeakHashMap;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

import com.zwitserloot.json.JSON;

import selvvir.AbstractSelvvirCommand.SelvvirCommandTerminated;

// suggestions: statcounter browser use rates, would be good additions to the bot
public class Selvvir extends PircBot {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String server = System.getProperty("server", "wolfe.freenode.net");
		String channels = System.getProperty("channel", "#pircbot");
		String name = System.getProperty("name", "Selvvir");
		String password = System.getProperty("password", null);
		String verbose = System.getProperty("verbose", "");
		String auth = System.getProperty("auth", "authenticated.properties");
		String googlekey = System.getProperty("googlekey", null);

		new Selvvir(server, channels.split(","), name, password, "verbose".equals(verbose), auth, googlekey);
	}

	public static final String ALIAS_PREFIX = "alias:";

	private static final String DIRTY_KEY = "dirty";
	private static final String INDEX_KEY = "index";
	private static final String FACTOIDS_KEY = "factoids";
	private static final String STATISTICS_KEY = "statistics";

	private static final Pattern URL_REGEX = Pattern
			.compile("(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))");

	protected static final String PREFIX = "`";

	private final String originalName;

	private final Map<String, AbstractSelvvirCommand> commands;
	private final Preferences factoids;
	private final Preferences statistics;
	private JSON index;

	private RuntimeProperties auth;

	private String lastUrl;

	private Field jsonObjectField;

	private boolean indexSynched;

	private Map<String, Void> talkedToTheBot;

	private Timer aggressiveTakeBackTimer;
	private String accountPassword;
	private String googlekey;

	public Selvvir(String server, String[] channels, String name, String password, boolean verbose, String authPath, String googlekey) {
		commands = new LinkedHashMap<String, AbstractSelvvirCommand>();

		statistics = Preferences.userNodeForPackage(getClass()).node(STATISTICS_KEY);
		factoids = Preferences.userNodeForPackage(getClass()).node(FACTOIDS_KEY);
		indexFactoidAliases();

		ServiceLoader<AbstractSelvvirCommand> loader = ServiceLoader.load(AbstractSelvvirCommand.class);
		Iterator<AbstractSelvvirCommand> itr = loader.iterator();

		try {
			auth = new RuntimeProperties(authPath);
		}
		catch (RuntimeException e) {
			e.printStackTrace();
			System.err.println("Admin commands will not be available.");
		}
		this.googlekey = googlekey;

		while (itr.hasNext()) {
			AbstractSelvvirCommand command = itr.next();
			commands.put(command.name(), command);
		}

		try {
			// As long as the library doesn't change, we're good!
			jsonObjectField = JSON.class.getDeclaredField("object");
			jsonObjectField.setAccessible(true);

			setVerbose(verbose);
			setName(name);
			setLogin("copy");
			setAutoNickChange(true);
			setEncoding("UTF-8");

			if (!server.isEmpty()) {

				if (password == null) {
					connect(server);
				}
				else {
					connect(server, 6667, password);
				}

				for (int i = 0; i < channels.length; i++) {
					joinChannel(channels[i]);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		originalName = name;
		talkedToTheBot = new WeakHashMap<String, Void>();

		aggressiveTakeBackTimer = new Timer(true);
		accountPassword = password;
	}

	private void indexFactoidAliases() {
		Preferences parent = factoids.parent();

		boolean dirty = parent.getBoolean(DIRTY_KEY, true);
		try {

			// Attempt to set dirty for any changes made while running.
			parent.putBoolean(DIRTY_KEY, true);
			parent.flush();

			if (!dirty) {

				index = JSON.parse(factoids.parent().get(INDEX_KEY, "{}"));
				indexSynched = true;
				return;
			}
			index = JSON.newMap();

			String[] factoids = getFactoids();
			if (factoids == null) {

				System.err.println("Building alias index failed!");
				return;
			}

			for (String from : factoids) {
				String to = getFactoid(from);

				if (to.startsWith("alias:")) {

					index.get(getAliasTarget(to)).add().setString(from);
				}
			}
			saveIndex(true);
		}
		catch (BackingStoreException e) {
			//
		}
	}

	private void saveIndex(boolean valid) {

		synchronized (index) {
			try {
				Preferences parent = factoids.parent();
				parent.put(INDEX_KEY, index.toJSON());

				if (valid) {
					parent.putBoolean(DIRTY_KEY, false);
				}
				parent.flush();

				if (valid) {
					indexSynched = true;
				}
			}
			catch (BackingStoreException e) {

				System.err.println("Unable to save index!");
				e.printStackTrace();
			}
		}
	}

	public String getGoogleKey() {
		if (googlekey != null) {
			return "?key=" + googlekey;
		}
		return "";
	}

	public Iterable<AbstractSelvvirCommand> commands() {
		return commands.values();
	}

	public String getAliasTarget(String alias) {
		return alias.substring(ALIAS_PREFIX.length());
	}

	public List<JSON> getAliases(String factoid) {
		synchronized (index) {
			return index.get(factoid).asList();
		}
	}

	private String getFactoid(String key, String def) {
		return factoids.get(key, factoids.get(def, null));
	}

	public String getFactoid(String key) {
		return factoids.get(key, null);
	}

	// If this fails with an error, the index will be out of date and when the bot closes down it won't know to fix it on next start.
	private void updateAliasIndex(String factoid, String value) throws Exception {
		boolean synched = indexSynched;

		if (value != null && value.startsWith(ALIAS_PREFIX)) {
			indexSynched = false;

			// The following code is definitely dangerous.
			String target = getAliasTarget(value);

			@SuppressWarnings("unchecked")
			Map<?, Object> object = (Map<?, Object>) jsonObjectField.get(index.get(target));

			@SuppressWarnings("unchecked")
			List<String> list = (List<String>) object.get(target);

			if (list != null) {
				Iterator<String> iterator = list.iterator();

				while (iterator.hasNext()) {

					if (factoid.equals(iterator.next())) {
						iterator.remove();
					}
				}
			}

			if (synched) {
				indexSynched = true;
			}
		}
	}

	public boolean setFactoid(String key, String value) {
		boolean success = false;

		synchronized (index) {
			try {
				String oldValue = factoids.get(key, null);

				factoids.put(key, value);
				factoids.flush();
				success = true;

				updateAliasIndex(key, oldValue);

				if (value.startsWith(ALIAS_PREFIX)) {

					index.get(getAliasTarget(value)).add().setString(key);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	public boolean deleteFactoid(String factoid) {
		boolean success = false;

		synchronized (index) {
			try {
				String value = factoids.get(factoid, null);

				factoids.remove(factoid);
				factoids.flush();
				success = true;

				updateAliasIndex(factoid, value);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return success;
	}

	public String[] getFactoids() {
		String[] keys = null;
		try {
			keys = factoids.keys();
		}
		catch (BackingStoreException e) {
			e.printStackTrace();
		}
		return keys;
	}

	private final void findURL(String message) {
		Matcher matches = URL_REGEX.matcher(message);

		while (matches.find()) {
			lastUrl = matches.group();
		}
	}

	public String getLastUrl() {
		return lastUrl;
	}

	public Set<Object> getAuthenticated() {

		if (auth != null) {
			return auth.getKeys();
		}
		return null;
	}

	public boolean isAuthenticated(String username) {
		return (auth != null && auth.getBoolean(username));
	}

	public void sendMessage(String channel, SelvvirResponses response, String... values) {

		sendMessage(channel, String.format(response.toString(), (Object[]) values));
	}

	public void sendNotice(String channel, SelvvirResponses response, String... values) {

		sendNotice(channel, String.format(response.toString(), (Object[]) values));
	}

	@Override
	protected void onConnect() {
		// Important! This prefixes all messages with + (for being logged in to an account) or - if not.
		sendRawLineViaQueue("CAP REQ identify-msg");
		sendRawLineViaQueue("CAP END");
	}

	@Override
	protected void onUserList(String channel, User[] users) {

		if (!originalName.equals(getNick())) {
			boolean found = false;

			for (User user : users) {
				if (originalName.equals(user.getNick())) {
					found = true;
					break;
				}
			}

			if (!found) {
				changeNick(originalName);
			}
		}
	}

	@Override
	protected void onMessage(String channel, String sender, String login, String hostname, String message) {

		handleMessage(channel, sender, login, hostname, message, false);
	}

	@Override
	protected void onPrivateMessage(String sender, String login, String hostname, String message) {

		if (message.equals("+``exit hyperspace") && ("AMcBain".equals(sender) || "Rainier".equals(sender))) {

			quitServer("bye!");
		}
		else {
			handleMessage(sender, sender, login, hostname, message, true);
		}
	}

	private void handleMessage(String channel, String sender, String login, String hostname, String message, boolean privateMsg) {

		boolean identified = "+".equals(message.substring(0, 1));
		message = message.substring(1);

		if (privateMsg && !message.startsWith(PREFIX)) {
			message = PREFIX + message;
		}

		if (message.startsWith(PREFIX)) {
			message = message.substring(PREFIX.length()).trim();

			if (message.isEmpty()) {
				return; // no reply
			}

			String target = sender;

			if (message.contains(" @ ")) {
				int at = message.indexOf(" @ ");
				String after = message.substring(at + 3);
				message = message.substring(0, at).trim();

				if (!after.isEmpty()) {
					target = after.trim();
				}
			}

			String command = message;
			String flags = "";
			int space = message.indexOf(' ');

			if (space != -1) {
				command = message.substring(0, space);
			}

			int slash = command.indexOf('/');
			if (slash != -1) {

				flags = command.substring(slash + 1);
				command = command.substring(0, slash);
			}

			AbstractSelvvirCommand cmd = commands.get(command);
			if (cmd != null) {
				message = (space == -1 ? "" : message.substring(space + 1)).trim();

				if (!cmd.isAdminOnly() || (isAuthenticated(sender) && identified)) {

					runCommand(cmd, flags, channel, sender, target, message);
				}
				else {

					sendNotice(sender, SelvvirResponses.ADMIN_ONLY);
				}
			}
			else {
				// Not a command so it doesn't need extra text like "`get "
				String value = getFactoid(message);

				if (value == null) {
					sendNotice(sender, SelvvirResponses.NOT_FOUND, message);
				}
				else {
					String key = value;

					if (value.startsWith("alias:")) {
						value = getAliasTarget(value);
						statistics.putInt(value, statistics.getInt(value, 0) + 1);
						value = getFactoid(value);
					}

					if (value == null) {
						sendNotice(sender, SelvvirResponses.BROKEN_ALIAS, message, key.split(":")[1]);
					}
					else {
						sendMessage(channel, target + ", " + value);
					}

					statistics.putInt(message, statistics.getInt(message, 0) + 1);
					try {
						statistics.flush();
					}
					catch (BackingStoreException e) {
						e.printStackTrace();
					}
				}
			}
		}
		else {
			findURL(message);
		}

		if (message.startsWith(getNick()) && !talkedToTheBot.containsKey(sender)) {

			String res = getFactoid("autobot", "bot");
			if (res != null) {

				// talkedToTheBot.put(sender, null);
				// sendMessage(channel, sender + ", " + res);
			}
		}
	}

	private void runCommand(AbstractSelvvirCommand cmd, String flags, String channel, String sender, String target, String message) {

		try {
			cmd.run(this, flags, channel, sender, target, message);
		}
		catch (SelvvirCommandTerminated t) {
			// this is their way of exiting on error and not processing anything more
			// makes their code cleaner, though this gives me a feeling of exception-based control/flow :(
		}
	}

	@Override
	protected void onQuit(String sourceNick, String sourceLogin, String sourceHostname, String reason) {

		if (originalName.equals(sourceNick)) {
			changeNick(originalName);
		}
	}

	@Override
	protected void onNickChange(String oldNick, String login, String hostname, String newNick) {

		if (originalName.equals(oldNick) && accountPassword != null) {

			aggressiveTakeBackTimer.schedule(new TimerTask() {

				private int sequence;

				@Override
				public void run() {
					switch (sequence) {
					case 0:
						sendMessage("NickServ", "identify " + originalName + " " + accountPassword);
						break;
					case 1:
						sendMessage("NickServ", "release " + originalName);
						break;
					case 2:
						changeNick(originalName);
						cancel();
					}
					sequence++;
				}
			}, 3000, 3000);
		}
	}

	@Override
	protected void onDisconnect() {

		saveIndex(indexSynched);
		dispose();
	}
}
