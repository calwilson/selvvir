package selvvir;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.zwitserloot.json.JSON;

public abstract class AbstractSelvvirCommand {

	private final String args;
	private final String info;
	private final Map<String, String> help;
	private final String keys;
	private String[] notes;
	private boolean adminOnly;

	public AbstractSelvvirCommand(String args, String info, String... help) {
		this.args = args;
		this.info = info;
		this.help = new LinkedHashMap<String, String>();
		notes = new String[0];

		String k = "";
		for (int i = 0; i < help.length; i += 2) {

			if (i + 1 < help.length) {
				this.help.put(help[i], help[i + 1]);
				k += help[i];
			}
		}
		keys = k;
	}

	protected void setNotes(String... notes) {

		if (notes != null) {
			this.notes = notes;
		}
	}

	protected void setAdminOnly() {
		adminOnly = true;
	}

	public boolean isAdminOnly() {
		return adminOnly;
	}

	public String name() {
		return getClass().getSimpleName().toLowerCase();
	}

	public String info() {
		return info;
	}

	protected void sendError(Selvvir bot, String username, SelvvirResponses response, String... values) {

		bot.sendNotice(username, response, values);
		throw new SelvvirCommandTerminated();
	}

	protected String truncate(String url) {

		if (url.length() > 31) {
			return url.substring(0, 20) + "…" + url.substring(url.length() - 10);
		}
		return url;
	}

	protected String shorten(String url, Selvvir bot, String sender, SelvvirResponses response, String... args) {

		JSON results = JSON.newMap();
		results.get("longUrl").add().setString(url);
		results = getResults("https://www.googleapis.com/urlshortener/v1/url" + bot.getGoogleKey(), results.toJSON());

		if (results == null) {
			sendError(bot, sender, response, args);
		}

		return results.get("id").asString();
	}

	public void run(Selvvir bot, String flags, String channel, String sender, String target, String text) {

		if (flags.contains("?")) {
			String pargs = (args.isEmpty() ? "" : args + " ");

			if (help.isEmpty()) {
				bot.sendNotice(target, SelvvirResponses.COMMAND_SYNTAX_NO_FLAGS, name(), pargs, info);
			}
			else {
				bot.sendNotice(target, SelvvirResponses.COMMAND_SYNTAX, name(), keys, pargs, info);
				bot.sendNotice(target, SelvvirResponses.COMMAND_FLAGS);
			}

			for (String key : help.keySet()) {
				bot.sendNotice(target, SelvvirResponses.COMMAND_FLAG, key, help.get(key));
			}

			for (String note : notes) {
				bot.sendNotice(target, SelvvirResponses.COMMAND_NOTE, note);
			}
		}
		else {
			runf(bot, flags, channel, sender, target, text);
		}
	}

	public abstract void runf(Selvvir bot, String flags, String channel, String sender, String target, String text);

	public static JSON getResults(String url, String postData) {
		JSON result = null;

		try {
			URL page = new URL(url);
			BufferedReader reader = null;
			OutputStreamWriter writer = null;

			try {
				URLConnection connection = page.openConnection();
				connection.setUseCaches(false);
				connection.addRequestProperty("Content-Type", "application/json");

				if (postData != null) {
					connection.setDoOutput(true);

					writer = new OutputStreamWriter(connection.getOutputStream());
					writer.write(postData);
					writer.flush();
				}
				connection.connect();

				if ("200".equals(connection.getHeaderField(0))) {

					reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

					StringBuilder builder = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
						builder.append("\n");
					}

					result = JSON.parse(builder.toString());
				}
				else {
					boolean writeOutput = false;

					if (connection instanceof HttpURLConnection) {
						InputStream stream = ((HttpURLConnection) connection).getErrorStream();

						if (stream != null) {
							writeOutput = true;
							reader = new BufferedReader(new InputStreamReader(stream));
						}
						else {
							reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
						}
					}

					StringBuilder builder = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
						builder.append("\n");
					}

					if (writeOutput) {
						System.err.println(builder.toString());
					}
					else {
						result = JSON.parse(builder.toString());
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if (page != null) {
					try {
						page.openStream().close();
					}
					catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				if (reader != null) {
					try {
						reader.close();
					}
					catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				if (writer != null) {
					try {
						writer.close();
					}
					catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/*
	 * The exit early class.
	 */
	public static class SelvvirCommandTerminated extends RuntimeException {

		private static final long serialVersionUID = 1L;
	}

}
